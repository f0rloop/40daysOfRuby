# Write a program that asks for a person’s first
# name, then middle, and then last. Finally, it should greet the
# person using their full name

puts "Hi there! What is your first name?"
fname = gets.chomp.capitalize
puts "What is your middle name?"
mname = gets.chomp.capitalize
puts "What is your last name?"
lname = gets.chomp.capitalize

puts "Oh hello #{fname} #{mname} #{lname}. I am pleased to meet you."