# Here’s something for you to do in order to play
# around more with center, ljust, and rjust: write a program that will
# display a table of contents 

title = "Table of Contents\n".center(60, ' ')
chapter1 = "Chapter 1:   Getting Started".ljust(50) + "page  1".rjust(0)
chapter2 = "Chapter 2:   Numbers".ljust(50) + "page  9".rjust(0)
chapter3 = "Chapter 3:   Letters".ljust(50) + "page  13".rjust(0)

puts title
puts chapter1
puts chapter2
puts chapter3

name = <<TOC
Chapter 4:   Operators 				  page  16
Chapter 5:   Control Flow			  page  19
Chapter 6:   File Structure			  page  26

TOC

puts  name