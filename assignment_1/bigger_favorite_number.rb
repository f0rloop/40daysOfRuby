# Write a program that asks for a person’s favorite number. Have your program add 1 to the number,
# and then suggest the result as a bigger and better favorite number.

puts "What is your favorite number?"
favnum = gets.chomp.to_i
betternum = favnum + 1
puts "Oh but #{betternum} is bigger and better."