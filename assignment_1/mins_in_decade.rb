# How many minutes are in a decade
MINS_IN_YEAR = (24 * 365) * 60
MINS_IN_DECADE = MINS_IN_YEAR * 10
puts "There are #{MINS_IN_YEAR} minutes in a year and #{MINS_IN_DECADE} minutes in a decade."
