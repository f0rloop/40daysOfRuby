#Some styling right here:
class String
def red;            "\033[31m#{self}\033[0m" end
def green;          "\033[32m#{self}\033[0m" end
def brown;          "\033[33m#{self}\033[0m" end
end

class Fixnum
def cyan;           "\033[36m#{self}\033[0m" end
def blue;           "\033[34m#{self}\033[0m" end
end

#GLOBAL VARIABLES
$file_a = File.open("added_tasks.txt", "a+")
$file_b = File.open("added_tasks.txt", "r")

#EDIT PER LINE
def edit_ln(line_num)
  puts "Enter new task"
  nt = gets.chomp
  d = Time.now
  date_edited = d.strftime("Modified on %m/%d/%Y")
  el_lines = IO.readlines("added_tasks.txt")
  el_lines[line_num-1] = nt.ljust(50) + "#{date_edited}"
  File.open("added_tasks.txt", 'w') do |file|
  file.puts(el_lines)
  # File.close("added_tasks.txt")
  end
end

#GENERATES THE LINE NUMBER
  def line_number
   $file_b.rewind 
   File.foreach("added_tasks.txt").with_index { |line, line_num|
   s = line_num += 1 
   puts "#{s.blue}: #{line}"
   }
  end

#FOOTER MENU GENERATOR
  def choose_next
    a = "A".green
    l = "L".green
    d = "D".green
    m = "M".green
    q = "Q".green
    e = "E".green

    puts "(#{a})dd Task | (#{l})ist |  (#{e})dit Task | (#{d})elete Task | (#{m})ain Menu | (#{q})uit".center(50,"----")
      option = gets.chomp.upcase
      case option
    when "A"
      add_task
    when "D"
      del_task
    when "E"
      edit_task
    when "L"
      list_task
    when "M"
      main_menu
    when "Q"
      exit
    else
      puts "Invalid option."
    end
  end

#ADD TASK METHOD
  def add_task
    task_list=Hash.new("Task Items")
  	puts "Enter new task:".brown
  	new_task = gets.chomp()
    d = Time.now
    date_created = d.strftime("Created on %m/%d/%Y")
    task_list={ 'Task' => new_task, 'Date' => date_created }
  	$file_a.puts(new_task.ljust(50) + "#{date_created}")
    $file_a.close()
    line_number
    puts "Task successfully added.\n"
    choose_next
  end

#DELETE TASK METHOD
  def del_task
    line_number
    puts "Enter line number to delete:"
    line_sel = gets.chomp.to_i
    File.open("added_tasks.txt", 'a+')
    el_lines = IO.readlines("added_tasks.txt")
    el_lines.delete_at(line_sel-1)
    temp_list = []
    temp_list << el_lines
    w = File.open("added_tasks.txt", "w")
    w.puts(temp_list)
    w.close
    choose_next
  end

#EDIT TASK METHOD
  def edit_task
   line_number
   puts "Enter line number to edit:".brown
   line_num = gets.chomp.to_i
   edit_ln(line_num)
   choose_next
  end

  # def mark_task
    # for next release  	
  # end

  def list_task
    line_number
    choose_next  
  end

  def main_menu
    title = "markIt - A command line to-do list.".red
    task = <<TASKLIST

  Choose Option
+  (A)dd task       
-  (D)elete task    
*  (E)dit task      
:  (L)ist
X  (C)ontact Developer     
TASKLIST
    puts title.center(60)
    puts task
    option = gets.chomp.upcase

    case option
    when "A"
      add_task
    when "D"
      del_task
    when "E"
      edit_task
    when "C"
      puts "Email: mark@bashfulcoder.com"
      choose_next
    when "L"
      list_task
    else
      puts "Invalid option."
    end
  end

main_menu
